# server/contracting_server.py

from sanic import Sanic, response
from sanic_cors import CORS
import ast

from contracting.db.encoder import encode
from contracting.client import ContractingClient
client = ContractingClient()

with open('../contracts/con_disperse_tau.py') as f:
        code = f.read()
        client.submit(code, name='con_disperse_tau')

app = Sanic("contracting server")
CORS(app)

@app.route("/transfer", methods=["POST",])
async def send_tau(request):
    # Get transaction details
    contract_name = request.json.get('contract')
    method_name = request.json.get('method')
    kwargs = request.json.get('args')
    sender = request.json.get('sender')

    # Set the sender
    client.signer = sender

    # Get reference to contract
    contract = client.get_contract(contract_name)

    # Return error of contract does not exist
    if contract_name is None:
        return response.json({'error': '{} does not exist'.format(contract)}, status=404)

    # Get reference to the contract method to be called
    method = getattr(contract, method_name)

    # Call method with supplied arguments and return status
    try:
        method(**kwargs)
        return response.json({'status': 0})
    except Exception as err:
        return response.json({'status': 1, 'error': str(err)})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=3737)
