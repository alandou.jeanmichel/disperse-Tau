import currency
S = Hash(default_value='')

@construct
def seed():
    S['name'] = 'Disperse Tau'
    S['description'] = 'Send Tau easily'

@export
def transferMultipletau(adress_amounts:dict):
    total_balance_to_Send = 0;
    sender = ctx.caller

    for receiverAdress in adress_amounts:
        total_balance_to_Send += int(adress_amounts[receiverAdress])

    assertBalanceEnougth(total_balance_to_Send)

    for receiverAdress in adress_amounts:
        price_amount = float(adress_amounts[receiverAdress])
        currency.transfer_from(amount=price_amount, to=receiverAdress, main_account=ctx.caller)


def assertBalanceEnougth(totalBalance:int):
    sender = ctx.caller
    balanceSender = currency.balance_of(sender)
    assert balanceSender >= totalBalance, "Transfer amount exceeds available token balance"



@export
def transfer(amount: float, to: str):
    assert amount > 0, 'Cannot send negative balances!'

    sender = ctx.caller

    assert balances[sender] >= amount, 'Not enough coins to send!'

    balances[sender] -= amount
    balances[to] += amount

@export
def balance_of(account: str):
    return balances[account]

@export
def allowance(owner: str, spender: str):
    return balances[owner, spender]

@export
def approve(amount: float, to: str):
    assert amount > 0, 'Cannot send negative balances!'

    sender = ctx.caller
    balances[sender, to] += amount
    return balances[sender, to]

@export
def transfer_from(amount: float, to: str, main_account: str):
    assert amount > 0, 'Cannot send negative balances!'

    sender = ctx.caller

    assert balances[main_account, sender] >= amount, 'Not enough coins approved to send! You have {} and are trying to spend {}'\
        .format(balances[main_account, sender], amount)
    assert balances[main_account] >= amount, 'Not enough coins to send!'

    balances[main_account, sender] -= amount
    balances[main_account] -= amount

    balances[to] += amount