import unittest

from contracting.client import ContractingClient
client = ContractingClient()
with open('./currency.py') as f:
    code = f.read()
    client.submit(code, name='currency')
with open('../con_disperse_tau.py') as f:
    code = f.read()
    client.submit(code, name='con_disperse_tau')

class MyTestCase(unittest.TestCase):
    def test_transfer(self):
        # set transaction sender
        client.signer = 'me'
        # Get contract reference

        my_token = client.get_contract('con_disperse_tau')
        # Call transfer method

        adress_amounts = {'you1': 10, 'you2': 10, 'you3': 10}

        my_token.transferMultipletau(adress_amounts=adress_amounts)
        # Assert token balance for 'me'
        self.assertEqual(my_token.quick_read('balances', 'me'), 20)
        # Assert token balance for 'you'
        self.assertEqual(my_token.quick_read('balances', 'you1'), 10)

if __name__ == '__main__':
    unittest.main()