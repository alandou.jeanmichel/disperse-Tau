import React, { useState, useEffect, useRef } from "react";
import { Button } from "react-rainbow-components";

function WalletButton(props) {
  const [installed, setInstalled] = useState(false);
  const [locked, setlocked] = useState(true);
  const [approved, setApproved] = useState(false);
  const [isLoadingApproveButton, setisLoadingApproveButton] = useState(false);
  const [isLoadingSendButton, setisLoadingSendButton] = useState(false);

  // const [name, setName] = useState("");
  // const [user, setUser] = useState("");
  const isFirstRun = useRef(true);

  useEffect(() => {
    if (window) {
      document.addEventListener("customInfo", (event) => {
        setInstalled(window.walletInstalled);
        if (window.walletInfo) {
          setlocked(window.walletInfo.locked);
          // setUser(window.walletInfo.wallets[0]);
        }
        console.log(event);
      });
    }
  });

  useEffect(() => {
    if (isFirstRun.current) {
      isFirstRun.current = false;
      return;
  }
    if(props.reset){
      if(approved){
        setApproved(false)
      }
    }
  // eslint-disable-next-line
  }, [props.reset]);

  const connectToWallet = () => {
    // var methodName = "set_value";
    // var networkType = "testnet";
    // var stampLimit = 50;
    // var argName, argType, argValue;
    var approveNetwork = "testnet";
    var approveContract = `${process.env.REACT_APP_CONTRACT_NAME}`;
    let reapprove = false;
    let newKeypair = false;

    const charms = [
      {
        formatAs: "string",
        iconPath: "/images/thumbsup.png",
        variableName: "yourState",
        key: "<wallet vk>:time",
        name: "Last Updated",
      },
    ];
    let detail = {
      appName: "Integration Testing",
      description:
        "Congrats! Your Dapp sent a successful approval request! Click APPROVE to create a dApp connection.",
      contractName: approveContract,
      networkType: approveNetwork,
      version: "1.1.0",
      preApproval: {
        stampsToPreApprove: 1000000,
        message: "Please pre-approve some stamps that we can use for testing.",
      },
      charms,
      logo: "/images/star.png",
      background: "/images/background.png",
    };
    if (reapprove) {
      detail.reapprove = true;
      if (newKeypair) detail.newKeypair = true;
    }
    window.connectToWallet(detail);
  };

  const sendTauToMultipleAdress = (addressToAmounts, handleApproveTx, callbackError) => {
    setisLoadingSendButton(true)
    let result = getAddressToAmount(addressToAmounts);
    debugger
    const transaction = {
      contractName: `${process.env.REACT_APP_CONTRACT_NAME}`,
      methodName: `${process.env.REACT_APP_METHOD_TRANSFERT}`,
      networkType: `${process.env.REACT_APP_NETWORKTYPE}`,
      stampLimit: parseInt(`${process.env.REACT_APP_STAMPLIMIT}`, 10),
      kwargs: {
        adress_amounts: { ...result },
      },
    };

    window.sendTx2(transaction, handleApproveTx, callbackError);
  };

  const getAddressToAmount = (addressToAmounts) => {
    let arrayAddress = addressToAmounts.split("\n");

    let totalAmount = 0;
    let addressToAmountsJson = {};

    arrayAddress.forEach((element) => {
      let values = element.split("=");
      addressToAmountsJson[values[0]] = values[1];
      if (values[1]) {
        totalAmount = totalAmount + parseFloat(parseInt(values[1], 10));
      }
    });

    return addressToAmountsJson;
  };

  const approveTransaction = (handleApproveTx, callbackError) => {
    setisLoadingApproveButton(true);
    const transaction = {
      contractName: `${process.env.REACT_APP_CONTRACT_CURRENCY}`,
      methodName: `${process.env.REACT_APP_METHOD_APPROVE}`,
      networkType: `${process.env.REACT_APP_NETWORKTYPE}`,
      stampLimit: parseInt(`${process.env.REACT_APP_STAMPLIMIT}`, 10),
      kwargs: {
        amount: parseFloat(parseInt(props.totalAmount, 10)),
        to: `${process.env.REACT_APP_CONTRACT_NAME}`,
      },
    };
    window.sendTx2(transaction, handleApproveTx, callbackError);
  };


  const openLink = () => {
    if (!installed) window.open("https://chrome.google.com/webstore/detail/lamden-wallet-browser-ext/fhfffofbcgbjjojdnpcfompojdjjhdim", "_blank");
  };

  const handleApproveTx = async (detail={}) => {
    if(detail.data && detail.data.resultInfo.errorInfo &&  detail.data.resultInfo.errorInfo.length > 0){
      props.setErrorMessage(detail.data.resultInfo.errorInfo)
    }
    let txInfo = detail.data.txInfo;
    if (txInfo.methodName === "approve") {
      setisLoadingApproveButton(false)
      setApproved(true);
    }
    if(txInfo.methodName === "transferMultipletau"){
      props.setTransactionLink(detail.data.txHash)
      setisLoadingSendButton(false)
      props.setReset(true)
    }
  };

  const callbackError = async (detail) => {
      debugger
      setisLoadingSendButton(false)
      setisLoadingApproveButton(false)
      props.setErrorMessage([''])
  }

  const walletStatusMessage = () => {
    if (installed) {
      if (locked) {
        return (
          <Button
            label="Wallet Locked"
            onClick={() => connectToWallet()}
            variant="brand"
            className="rainbow-m-around_medium"
          />
        );
      } else {
        return (
          <div>
            <Button
              label="Appove Tau"
              isLoading={isLoadingApproveButton}
              onClick={() => approveTransaction(handleApproveTx, callbackError)}
              variant="brand"
              disabled={
                approved || !props.totalAmount || props.totalAmount === 0
              }
              className="rainbow-m-around_medium"
            />
            <Button
              label="Send Tau"
              isLoading={isLoadingSendButton}
              disabled={!approved}
              onClick={(val) =>
                sendTauToMultipleAdress(props.addressToAmounts, handleApproveTx, callbackError)
              }
              variant="brand"
              className="rainbow-m-around_medium"
            />
          </div>
        );
      }
    } else {
      return (
        <Button
          label="Install Lamden Wallet"
          onClick={() => openLink()}
          variant="brand"
          className="rainbow-m-around_medium"
        />
      );
    }
  };

  return (
    <div>
      <div>{walletStatusMessage()}</div>
    </div>
  );
}

export default WalletButton;
