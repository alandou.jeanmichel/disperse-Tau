import styled from 'styled-components';

export const Wrapper = styled.div`
    overflow: hidden;
    margin-bottom: 32px;
    transition: all 300ms ease;
    color: rgba(0, 0, 0, 0.85);
    text-align: center;
    /* stylelint-disable-next-line value-keyword-case */
    ${props => props.searchState && props.searchState.query && 'overflow: visible;'}

    @media screen and (max-width: 991px) {
        margin-bottom: 16px;
    }
`;

export const PresentationWrapper = styled.div`
    margin-bottom: 36px;
    padding-top: 59px;
    transition: all 300ms ease;
    margin-top: 36px;
    /* stylelint-disable-next-line value-keyword-case */
    ${props => props.searchState && props.searchState.query && 'padding-top: 0; margin-top: -50px;'}

    @media screen and (max-width: 767px) {
        padding-top: 49px;
    }

    @media screen and (max-width: 479px) {
        padding-top: 39px;
    }
`;

export const IntroLabel = styled.h1`
    margin-top: 0;
    transition: all 350ms ease;
    font-family: Montserrat, sans-serif;
    font-size: 22px;
    line-height: 1em;
    font-weight: 400;
    margin-bottom: 29px;
    letter-spacing: 1.6px;
    /* stylelint-disable-next-line value-keyword-case */
    ${props =>
        props.searchState && props.searchState.query && 'margin-bottom: 0px;font-size: 32px;'}
    color: ${props => props.theme.rainbow.palette.text.main};

    @media screen and (max-width: 991px) {
        font-size: 54px;
    }

    @media screen and (max-width: 767px) {
        font-size: 44px;
    }

    @media screen and (max-width: 479px) {
        font-size: 34px;
    }
`;

export const RecentItem = styled.a`
    padding-right: 25px;
    background-image: url('/assets/icons/forward-arrow-curved.svg');
    background-position: 100% 50%;
    background-size: 16px 16px;
    background-repeat: no-repeat;
    margin-left: 5px;

    @media screen and (max-width: 767px) {
        background-size: 12px 12px;
    }
`;
