import React, { useState, useEffect } from "react";
import { ContainerMedium } from "../../styled";
import {
  Textarea
} from "react-rainbow-components";
import WalletButton from "../../components/wallet/WalletButton";
import { Table } from "semantic-ui-react";
import {v4 as uuidv4} from "uuid"

import {
  Wrapper,
  PresentationWrapper,
  IntroLabel,
} from "./styled";
const containerStyles = {
  maxWidth: 900,
};

function Home() {
  const [addressToAmounts, setAdressWithAmount] = useState("");
  const [totalAmount, setTotalAmount] = useState(0);
  const [users, setUsers] = useState([]);
  const [fieldHasError, setFieldHasError] = useState(false);
  const [totalAdresses, settotalAdresses] = useState(0);
  const [maxAmountTransfert, setMaxAmountTransfert] = useState(0);
  const [transactionLink, setTransactionLink] = useState("");
  const [errors, setErrorMessages] = useState([]);
  const [reset, setReset] = useState('');

  const setTransactionHash = (hash) => {
    setTransactionLink(hash)
  }

  const setErrorMessage = (errors) => {
    setErrorMessages(errors)
  }


  const handleChange = (data) => { 
    setAdressWithAmount(data)
    setReset(uuidv4())
    setErrorMessages([])
  }

  const getMaxStampFee = () => {
    fetch("https://mainnet.lamden.io/api/stamps/currency/transfer")
      .then((response) => response.json())
      .then((data) => {
        debugger
        setMaxAmountTransfert(data.max)
        console.log(data); // Prints result from `response.json()` in getRequest
      })
      .catch((error) => console.error(error));
  };

  useEffect(() => {
    let arrayAddress = addressToAmounts.split("\n");
    let innertotalAmount = 0;
    let innerTotaladresses = 0;
    setFieldHasError(false);
    setTotalAmount(0);
    settotalAdresses(0);
    getMaxStampFee();

    let addressToAmountsJson = [];

    arrayAddress.forEach((element) => {
      let values = element.split("=");
      if (values.length === 2 && values[0] && values[1]) {
        if (typeof values[0] !== "string") {
          setFieldHasError(true);
          return;
        }
        let amount = parseFloat(parseInt(values[1], 10));

        if (isNaN(amount)) {
          setFieldHasError(true);
          return;
        }
        addressToAmountsJson.push({
          index: innerTotaladresses,
          address: values[0],
          amount: amount,
        });
        innertotalAmount += amount;
        innerTotaladresses++;
      }
      setUsers(addressToAmountsJson);
    });
    addressToAmountsJson.forEach((element) => {
      settotalAdresses(innerTotaladresses);
      setTotalAmount(innertotalAmount);
    });
  }, [addressToAmounts]);

  return (
    <ContainerMedium>
      <Wrapper>
        <PresentationWrapper>
          <IntroLabel>Recipients and amounts</IntroLabel>
          <Textarea
            error={fieldHasError && "field(s) error"}
            id="example-textarea-1"
            onChange={(e) => handleChange(e.target.value)}
            rows={6}
            placeholder="adress=amount"
            style={containerStyles}
            className="rainbow-m-vertical_x-large rainbow-p-horizontal_medium rainbow-m_auto"
          />
          <IntroLabel>Confirm</IntroLabel>
          <div
            style={containerStyles}
            className="rainbow-m-vertical_x-large rainbow-p-horizontal_medium rainbow-m_auto"
          >
            <Table size="large">
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>#</Table.HeaderCell>
                  <Table.HeaderCell>Adresse</Table.HeaderCell>
                  <Table.HeaderCell>Amount</Table.HeaderCell>
                </Table.Row>
              </Table.Header>

              <Table.Body>
                {users.map((user, index) => (
                  <Table.Row key={index}>
                    <Table.Cell>{user.index}</Table.Cell>
                    <Table.Cell>{user.address}</Table.Cell>
                    <Table.Cell>{user.amount}</Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
              <Table.Footer>
                <Table.Row>
                  <Table.HeaderCell />
                  <Table.HeaderCell>
                    {totalAdresses} Adress(es)
                  </Table.HeaderCell>
                  <Table.HeaderCell>
                    <h2>Fees {maxAmountTransfert*totalAdresses} Stamps</h2>
                    <h2>Total {totalAmount+(maxAmountTransfert*totalAdresses)/1000} Tau</h2>
                    </Table.HeaderCell>
                </Table.Row>
              </Table.Footer>
            </Table>
          </div>
          {transactionLink !== '' && <a style={{'color':'rgb(68, 215, 182);'}} href={`https://testnet.lamden.io/transactions/${transactionLink}`}>{transactionLink}</a>}
          {errors.length !== 0 && <h2 style={{'color':'#fe4849'}}>{errors[0]}</h2>}
          <WalletButton
            addressToAmounts={addressToAmounts}
            totalAmount={totalAmount}
            setTotalAmount={setTotalAmount}
            setTransactionLink={setTransactionHash}
            setErrorMessage={setErrorMessage}
            reset={reset}
            setReset={setReset}
          ></WalletButton>
        </PresentationWrapper>
      </Wrapper>
    </ContainerMedium>
  );
}

export default Home;
