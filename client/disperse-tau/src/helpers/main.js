export const formatAccountAddress = (account, lsize = 8, rsize = 4) => {
    return account.substring(0,lsize) + '...' + account.substring(account.length - rsize)
}