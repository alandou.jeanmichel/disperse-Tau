export const config = {
    domainName: "https://pixelframes.lamden.io",
    blockExplorer: "https://testnet.lamden.io/api",
    masternode: "https://testnet-master-1.lamden.io"
}